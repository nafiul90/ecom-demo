const config = require("config");
const express = require("express");
const app = express();
const customers = require("./routes/customer");
const auth = require("./routes/auth");
const product = require("./routes/product");
const productVariant = require("./routes/productVariant");
const path = require("path");

if (!config.get("jwtPrivateKey")) {
  console.error("FATAL ERROR: jwtPrivate key is not define");
  process.exit(1);
}

app.use(`${__dirname}`, express.static(path.join(__dirname)));

require("./db")();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:3001"); // update to match the domain you will make the request from
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.use(express.json());
app.use("/api/customers", customers);
app.use("/api", auth);
app.use("/api/product", product);
app.use("/api/variant", productVariant);

const port = process.env.PORT || 3000;
app.listen(port, () => console.log("Listenig to port ", port));
