const mongoose = require("mongoose");

module.exports = () =>
  mongoose
    .connect("mongodb://localhost/ecom-demo", { useNewUrlParser: true })
    .then(() => console.log("Connected to mongodb..."))
    .catch(err => console.log("Could not connect to mongodb..."));
