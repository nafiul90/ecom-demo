const mongoose = require("mongoose");
const Joi = require("joi");
Joi.ObjectId = require("joi-objectid")(Joi);

const Variant = mongoose.Schema({
  attributes: {},
  image: {
    type: String,
    required: true
  },
  quantity: {
    type: Number,
    min: 0,
    max: 999999,
    required: true
  },
  price: {
    type: Number,
    min: 0,
    max: 999999,
    required: true
  }
});

const ProductVariant = mongoose.model(
  "ProductVariant",
  new mongoose.Schema({
    productId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true
    },
    variants: [
      {
        type: Variant
      }
    ]
  })
);

const variantsValidator = Joi.object().keys({
  attributes: Joi.object(),
  image: Joi.string()
    .min(5)
    .max(2000)
    .required(),
  quantity: Joi.string()
    .min(0)
    .max(99999),
  price: Joi.string()
    .min(0)
    .max(9999999)
});

const validateProductVariant = function(value) {
  const schema = {
    productId: Joi.ObjectId().required(),
    variants: Joi.array().items(variantsValidator)
  };

  return Joi.validate(value, schema, { abortEarly: false });
};

module.exports = { ProductVariant, validateProductVariant };
