const Joi = require("joi");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const config = require("config");

const customerSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 10
  },
  password: {
    type: String,
    required: true,
    minlength: 6,
    maxlength: 2000
  },
  name: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  },
  isGold: {
    type: Boolean,
    default: false
  },
  isAdmin: {
    type: Boolean,
    default: false
  },
  phone: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  }
});

customerSchema.methods.generateAuthToken = function() {
  const token = jwt.sign(
    { _id: this._id, isGold: this.isGold, username: this.username },
    config.get("jwtPrivateKey")
  );
  return token;
};
const Customer = mongoose.model("Customer", customerSchema);

function validateCustomer(customer) {
  const schema = {
    username: Joi.string()
      .min(3)
      .max(10)
      .required(),
    password: Joi.string()
      .min(6)
      .max(2000)
      .required(),
    name: Joi.string()
      .min(5)
      .max(50)
      .required(),
    phone: Joi.string()
      .min(5)
      .max(50)
      .required(),
    isGold: Joi.boolean()
  };

  return Joi.validate(customer, schema, { abortEarly: false });
}

exports.Customer = Customer;
exports.validateCustomer = validateCustomer;
