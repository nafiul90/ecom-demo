const Joi = require("joi");
const mongoose = require("mongoose");
Joi.ObjectId = require("joi-objectid")(Joi);

const Product = mongoose.model(
  "Product",
  new mongoose.Schema({
    _id: {
      type: mongoose.Schema.Types.ObjectId,
      required: true
    },
    name: {
      type: String,
      required: true,
      minlength: 3,
      maxlength: 20
    },
    department: {
      type: String,
      minlength: 3,
      maxlength: 20,
      require: true
    },
    category: {
      type: [String],
      required: true
    },
    brand: {
      type: String,
      minlength: 2,
      maxlength: 100
    },
    thumbnailImage: {
      type: String,
      minlength: 3,
      required: true
    }
  })
);

// function rangeArrayLimit(val) {
//   return val.length === 2;
// }

function validateProduct(product) {
  const schema = {
    _id: Joi.ObjectId().required(),
    name: Joi.string()
      .min(3)
      .max(20),
    department: Joi.string()
      .min(3)
      .max(20),
    category: Joi.array()
      .items(Joi.string())
      .required(),
    brand: Joi.string()
      .min(3)
      .max(20),
    thumbnailImage: Joi.string()
      .min(5)
      .max(2000)
  };

  return Joi.validate(product, schema, { abortEarly: false });
}

module.exports = { Product, validateProduct };
