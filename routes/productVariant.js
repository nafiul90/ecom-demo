const {
  getProductVariantsByProductId
} = require("../controllers/productVariant");
const express = require("express");
const router = express.Router();

router.get("/:id", getProductVariantsByProductId);

module.exports = router;
