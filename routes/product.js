const express = require("express");
const router = express.Router();
const { addProduct, getProducts } = require("../controllers/product");

const fs = require("fs");
const path = require("path");
const app = express();

const multer = require("multer");

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    let date = new Date();
    date = "" + date.getDate() + date.getMonth() + date.getFullYear();
    const uploadDir = path.join(
      __dirname,
      "..",
      "public",
      "uploads",
      "products",
      `${date}`
    );
    fs.mkdir(uploadDir, function(e) {
      if (!e || (e && e.code === "EEXIST")) {
        cb(null, uploadDir);
      } else {
        console.log("File creation faild,", e);
      }
    });
  },
  filename: (req, file, cb) => {
    cb(
      null,
      file.fieldname + "-" + new Date().toISOString() + "-" + file.originalname
    );
  }
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg" ||
    file.mimetype === "image/jpeg"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

var upload = multer({ storage: fileStorage, fileFilter: fileFilter }).fields([
  {
    name: "thumbnailImages",
    maxCount: 1
  },
  {
    name: "images",
    maxCount: 100
  }
]);

router.post("/", upload, addProduct);
router.get("/", getProducts);

module.exports = router;
