const express = require("express");
const router = express.Router();
const { signup, getCustomers } = require("../controllers/customer");
const admin = require("../middleware/admin");
const auth = require("../middleware/auth");

router.get("/", [auth], getCustomers);
router.post("/signup", signup);

module.exports = router;
