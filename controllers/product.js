const { Product, validateProduct } = require("../models/product");
const {
  ProductVariant,
  validateProductVariant
} = require("../models/productVariant");
const mongoose = require("mongoose");
const _ = require("lodash");

exports.addProduct = async (req, res) => {
  // const {error} = validateProduct(req.body);
  // if(error) return res.status(400).send(error.details.map(e=>e.message));

  try {
    console.log("files", req.files);
    let productId = mongoose.Types.ObjectId().toString();

    let thumbnailImage = req.files.thumbnailImages[0].path;

    //saperate variants
    let variants = JSON.parse(req.body.variants);

    for (let i = 0; i < variants.length; i++) {
      variants[i].image = req.files.images[i].path;
    }
    console.log("variants===>", variants);

    let product = JSON.parse(req.body.product);

    product._id = productId;
    product.thumbnailImage = thumbnailImage;
    delete product.image;
    console.log("product===>", product);

    const { error } = validateProduct(product);
    if (error) {
      console.log(error);
      return res.status(400).send(error.details.map(e => e.message));
    }

    product = new Product(product);
    await product.save();

    let productVariant = { productId, variants };

    const { error2 } = validateProductVariant(productVariant);
    if (error2) {
      console.log(error2);
      return res.status(400).send(error2.details.map(e => e.message));
    }

    productVariant = new ProductVariant(productVariant);
    await productVariant.save();
  } catch (err) {
    console.log(err);
  }
};

exports.getProducts = async function(req, res) {
  try {
    let products = await Product.find();
    return res.status(200).send(products);
  } catch (err) {
    console.log(err);
    return res.status(500).send("Internal server error");
  }
};
