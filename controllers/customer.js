const { Customer, validateCustomer } = require("../models/customer");
const bcryptjs = require("bcryptjs");
const _ = require("lodash");

exports.signup = async (req, res) => {
  try {
    const { error } = validateCustomer(req.body);
    if (error) return res.status(400).send(error.details.map(e => e.message));

    let user = await Customer.findOne({ username: req.body.username });
    if (user) return res.status(400).send("Username already exists.");

    let customer = new Customer(req.body);
    const salt = await bcryptjs.genSalt(10);
    customer.password = await bcryptjs.hash(customer.password, salt);
    customer = await customer.save();
    return res.status(200).send(customer);
  } catch (err) {
    res.status(500).send(err);
  }
};

exports.getCustomers = async (req, res) => {
  try {
    console.log("get all customer");
    let customers = await Customer.find().select("-password");
    return res.status(200).send(customers);
  } catch (err) {
    return res.status(500).send(err);
  }
};
