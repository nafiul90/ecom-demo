const { ProductVariant } = require("../models/productVariant");

exports.getProductVariantsByProductId = async function(req, res) {
  try {
    let id = req.params.id;
    let variants = await ProductVariant.findOne({ productId: id });
    // let variants = await ProductVariant.find();
    return res.status(200).send(variants);
  } catch (error) {
    return res.status(500).send("Internal server error.");
  }
};
