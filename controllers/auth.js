const Joi = require("joi");
const bcryptjs = require("bcryptjs");
const { Customer } = require("../models/customer");

exports.login = async (req, res) => {
  try {
    const { error } = validateLogin(req.body);
    if (error) return res.status(400).send(error.details.map(e => e.message));

    let customer = await Customer.findOne({ username: req.body.username });
    if (!customer) return res.status(400).send("Invalid user or password");

    const validPassword = await bcryptjs.compare(
      req.body.password,
      customer.password
    );
    if (!validPassword) return res.status(400).send("Invalid user or password");

    const token = customer.generateAuthToken();
    res.status(200).send(token);
  } catch (err) {
    console.log(err);
    res.status(500).send("Internal server error");
  }
};

function validateLogin(value) {
  const schema = {
    username: Joi.string()
      .min(3)
      .max(10),
    password: Joi.string()
      .min(6)
      .max(20)
  };
  return Joi.validate(value, schema, { abortEarly: false });
}
